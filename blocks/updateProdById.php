<?php
#========================= setting ===========================#
include_once (dirname(__DIR__).'/libs/crest/CRest.php');
include_once (dirname(__DIR__).'/libs/debugger/Debugger.php');
define ('PATH', dirname(__DIR__).'/log/blockslog.txt');
define ('LOG', false);
#=============================================================#
### Блок который позволяет изменить товары по ID ###
Debugger::writeToLog($_REQUEST, PATH, 'updateProdById:Новый запрос', LOG);
if (isset($_REQUEST['code']) && $_REQUEST['code'] == 'updateProdById') {
	if (is_array($_REQUEST['properties']['id'])) {
		foreach ($_REQUEST['properties']['id'] as $id) {
			$product = CRest::call('crm.product.update', array(
				'id' => $id,
				'fields' => array($_REQUEST['properties']['field'] => $_REQUEST['properties']['newValue'])
			));
		}
	} else {
		$product = CRest::call('crm.product.update', array(
			'id' => $_REQUEST['properties']['id'],
			'fields' => array($_REQUEST['properties']['field'] => $_REQUEST['properties']['newValue'])
		));
	}
	Debugger::writeToLog($product, PATH, 'updateProdById:Обновили поля продукта', LOG);

	### ответ ###
	$result = isset($product['result']) ? 'Y' : 'N';
	$params = array(
		'EVENT_TOKEN'   => $_REQUEST['event_token'],
		'RETURN_VALUES' => array('outputString' => $result)
	);
	$answer = CRest::call('bizproc.event.send', $params);
	Debugger::writeToLog($answer, PATH, 'prodFromDeal:Ответ процессу', LOG);
}