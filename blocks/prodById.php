<?php
#========================= setting ===========================#
include_once (dirname(__DIR__).'/libs/crest/CRest.php');
include_once (dirname(__DIR__).'/libs/debugger/Debugger.php');
define ('PATH', dirname(__DIR__).'/log/blockslog.txt');
define ('LOG', false);
#=============================================================#
### Блок который позволяет получить товары по ID ###
Debugger::writeToLog($_REQUEST, PATH, 'prodById:Получили запрос', LOG);
if (isset($_REQUEST['code']) && $_REQUEST['code'] == 'prodById') {
	Debugger::writeToLog($_REQUEST, PATH, 'Данные поля', LOG);

	$product = CRest::call('crm.product.list', array(
		'filter' => array('ID' => $_REQUEST['properties']['id']),
		'select' => array($_REQUEST['properties']['field'])
	));
	Debugger::writeToLog($product, PATH, 'prodById:Получили данные товара', LOG);

	### ответ ###
	foreach ($product['result'] as $value) {
		if (is_array($value[$_REQUEST['properties']['field']])) {
			foreach ($value[$_REQUEST['properties']['field']] as $v) {
				$var[] = is_array($v) ? array_pop($v) : $v;
			}
		} else {
			$var[] = $value[$_REQUEST['properties']['field']];
		}
	}
	Debugger::writeToLog($var, PATH, 'VAR', LOG);

	$params = array(
		'EVENT_TOKEN'   => $_REQUEST['event_token'],
		'RETURN_VALUES' => array('output' => '2019-12-04T15:10:47')
	);
	$answer = CRest::call('bizproc.event.send', $params);
	Debugger::writeToLog($answer, PATH, 'prodById:Ответ процессу', LOG);
}