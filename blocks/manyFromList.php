<?php
#========================= setting ===========================#
include_once (dirname(__DIR__).'/libs/crest/CRest.php');
include_once (dirname(__DIR__).'/libs/debugger/Debugger.php');
define ('PATH', dirname(__DIR__).'/log/blockslog.txt');
define ('LOG', true);
#=============================================================#
### Блок который позволяет получить массив данных из универсального списка ###
Debugger::writeToLog($_REQUEST, PATH, 'manyFromList:Получили запрос', LOG);
if (isset($_REQUEST['code']) && $_REQUEST['code'] == 'manyFromList') {
	$list = CRest::call('lists.element.get', array(
		'IBLOCK_TYPE_ID' => 'lists',
		'IBLOCK_ID'      => $_REQUEST['properties']['id'],
		'FILTER'         => array($_REQUEST['properties']['fieldIdForFilter'] => $_REQUEST['properties']['filter'])
	));
	Debugger::writeToLog($list, PATH, 'manyFromList:Получили списки', LOG);

	### ответ ###
	foreach ($list['result'] as $value) {
		foreach ($value as $k => $v) {
			$var[] = array($k.'='.$v);
		}
	}

	$params = array(
		'EVENT_TOKEN'   => $_REQUEST['event_token'],
		'RETURN_VALUES' => array('output' => $var)
	);
	Debugger::writeToLog($params, PATH, 'manyFromList:Параметры ответа', LOG);
	$answer = CRest::call('bizproc.event.send', $params);
	Debugger::writeToLog($answer, PATH, 'manyFromList:Ответ процессу', LOG);
}