<?php
#========================= setting ===========================#
include_once (dirname(__DIR__).'/libs/crest/CRest.php');
include_once (dirname(__DIR__).'/libs/debugger/Debugger.php');
define ('PATH', dirname(__DIR__).'/log/blockslog.txt');
define ('LOG', false);
#=============================================================#
### Блок который позволяет получить товары из сделки ###
Debugger::writeToLog($_REQUEST, PATH, 'prodFromDeal:Новый запрос', LOG);
if (isset($_REQUEST['code']) && $_REQUEST['code'] == 'prodFromDeal') {
	$product = CRest::call('crm.deal.productrows.get', array('id' => $_REQUEST['properties']['id']));
	Debugger::writeToLog($product, PATH, 'prodFromDeal:Получили товар', LOG);

	for ($i = 0, $s = count($product['result']); $i < $s; $i++) {
		$productList[] = $product['result'][$i]['PRODUCT_ID'];
	}
	Debugger::writeToLog($productList, PATH, 'prodFromDeal:Собираем ID', LOG);

	### ответ ###
	$params = array(
		'EVENT_TOKEN' => $_REQUEST['event_token'],
		'RETURN_VALUES' => array('output' => $productList)
	);
	$answer = CRest::call('bizproc.event.send', $params);
	Debugger::writeToLog($answer, PATH, 'prodFromDeal:Ответ процессу', LOG);
}