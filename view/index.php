<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="view/css/main.css">
	</head>
	<body>
		<form method="GET">
			<?php foreach($blocks as $item => $desc): ?>
				<div class='block'>
					<p><span class='item'>Название: </span><?=$desc['name']?><br><span class='item'>Описание: </span><?=$desc['description']?></p>
					<input class='ui-btn ui-btn-success' type="submit" value="<?=isset($status[$item]) ? $status[$item] : 'Установить'?>" name="<?=$item?>">
				</div>
			<?php endforeach; ?>
		</form>
	</body>
</html>