<?php
include_once __DIR__.'/libs/crest/CRest.php';
include_once __DIR__.'/libs/debugger/Debugger.php';
include_once __DIR__.'/blocks.php';
define ('CLIENT', __DIR__.'/libs/crest/settings.json');
define ('PATH', __DIR__.'/log/log.txt');
define ('HANDLER', 'http://9f6f962b.ngrok.io/projects/tpk_osnova/blocks/');  // путь до блока-обработчика
#=======================================================================================================#
if (isset($_REQUEST['PLACEMENT']) && !file_exists(CLIENT)) {
	$install = CRest::installApp(); // установка приложения
	header("Location: index.php");
	
} elseif (in_array(key($_REQUEST), array_keys($blocks))) {
	if ($_REQUEST[key($_REQUEST)] == 'Установить') {
		$params = require ('setting.php');
		$result = CRest::call('bizproc.activity.add', $params[key($_REQUEST)]); // установка действия

	} elseif ($_REQUEST[key($_REQUEST)] == 'Удалить') {
		$result = CRest::call('bizproc.activity.delete', array('CODE' => key($_REQUEST))); // удаление действия
	}
	header("Location: index.php");

} else {
	$bizActivityList = CRest::call('bizproc.activity.list', array());
	if (isset($bizActivityList['result'])) {
		foreach ($bizActivityList['result'] as $activ) {
			if (in_array($activ, array_keys($blocks))) $status[$activ] = 'Удалить';
		}
	}
	include_once (__DIR__.'/view/index.php'); // view
}