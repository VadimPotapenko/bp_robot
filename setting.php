<?php
return array(
	### handler устанавливается в индексном файле (просто копируем значение,в тч и значение name) ###
	### действие получить товары из сделки ###
	'prodFromDeal'   => array(
		'CODE'             => key($_REQUEST),
		'HANDLER'          => HANDLER.key($_REQUEST).'.php',
		'NAME'             => $blocks[key($_REQUEST)]['name'],
		'AUTH_USER_ID'     => '1',
		'USE_SUBSCRIPTION' => 'Y',
		'PROPERTIES'       => array(
			'id' => array(
				'Name'     => 'ID сущности',
				'Type'     => 'string',
				'Default'  => '{=Document:ID}',
				'Required' => 'Y'
			)
		),
		'RETURN_PROPERTIES' => array(
			'output' => array(
				'Name'     => 'Массив значений',
				'Type'     => 'double',
				'Multiple' => 'Y'
			)
		)
	),
	### получить товар по id ###
	'prodById'       => array(
		'CODE'             => key($_REQUEST),
		'HANDLER'          => HANDLER.key($_REQUEST).'.php',
		'NAME'             => $blocks[key($_REQUEST)]['name'],
		'AUTH_USER_ID'     => '1',
		'USE_SUBSCRIPTION' => 'Y',
		'PROPERTIES'       => array(
			'id' => array(
				'Name'     => 'ID товара',
				'Type'     => 'string',
				'Required' => 'Y'
			),
			'field' => array(
				'Name'     => 'Код поля товара',
				'Type'     => 'string',
				'Required' => 'Y'
			)
		),
		'RETURN_PROPERTIES' => array(
			'output' => array(
				'Name'     => 'Массив значений',
				'Type'     => 'string',
				'Multiple' => 'Y'
			)
		)
	),
	### данные поля из универсальных списков по фильтру ###
	'oneFromList'  => array(
		'CODE'             => key($_REQUEST),
		'HANDLER'          => HANDLER.key($_REQUEST).'.php',
		'NAME'             => $blocks[key($_REQUEST)]['name'],
		'AUTH_USER_ID'     => '1',
		'USE_SUBSCRIPTION' => 'Y',
		'PROPERTIES'       => array(
			'id'            => array(
				'Name'     => 'ID списка',
				'Type'     => 'string',
				'Required' => 'Y'
			),
			'fieldIForFilter' => array(
				'Name'     => 'ID поля для фильтра',
				'Type'     => 'string',
				'Required' => 'Y'
			),
			'filter'        => array(
				'Name'     => 'Значение поля для фильтрации',
				'Type'     => 'string',
				'Required' => 'Y'
			),
			'needFieldById' => array(
				'Name'     => 'ID полей, которые хотим вернуть',
				'Type'     => 'string',
				'Required' => 'Y'
			)
		),
		'RETURN_PROPERTIES' => array(
			'output' => array(
				'Name'     => 'Данные из универсального списка',
				'Type'     => 'string',
				'Multiple' => 'Y'
			)
		)
	),
	### данные из универсального списка по фильтру ###
	'manyFromList' => array(
		'CODE'             => key($_REQUEST),
		'HANDLER'          => HANDLER.key($_REQUEST).'.php',
		'NAME'             => $blocks[key($_REQUEST)]['name'],
		'AUTH_USER_ID'     => '1',
		'USE_SUBSCRIPTION' => 'Y',
		'PROPERTIES'       => array(
			'id'               => array(
				'Name'     => 'ID списка',
				'Type'     => 'string',
				'Required' => 'Y'
			),
			'fieldIdForFilter' => array(
				'Name'     => 'ID поля для фильтрации',
				'Type'     => 'string',
				'Required' => 'Y'
			),
			'filter'           => array(
				'Name'     => 'Значение поля для фильтрации',
				'Type'     => 'string',
				'Required' => 'Y'
			),
		),
		'RETURN_PROPERTIES' => array(
			'output' => array(
				'Name'     => 'Данные из универсального списка',
				'Type'     => 'string',
				'Multiple' => 'Y'
			)
		)
	),
	### обновление товара по ID ###
	'updateProdById' => array(
		'CODE'             => key($_REQUEST),
		'HANDLER'          => HANDLER.key($_REQUEST).'.php',
		'NAME'             => $blocks[key($_REQUEST)]['name'],
		'AUTH_USER_ID'     => '1',
		'USE_SUBSCRIPTION' => 'Y',
		'PROPERTIES'       => array(
			'id'       => array(
				'Name'     => 'ID товара',
				'Type'     => 'string',
				'Required' => 'Y'
			),
			'field'    => array(
				'Name'     => 'Код поля товара',
				'Type'     => 'string',
				'Required' => 'Y'
			),
			'newValue' => array(
				'Name'     => 'Новое значение',
				'Type'     => 'string',
				'Required' => 'Y'
			),
		),
		'RETURN_PROPERTIES' => array(
			'outputString' => array(
				'Name' => 'Результат',
				'Type' => 'string',
			)
		)
	),
);